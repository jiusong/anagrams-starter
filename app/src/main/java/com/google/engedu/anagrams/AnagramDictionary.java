/* Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.engedu.anagrams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class AnagramDictionary {

    private static final int MIN_NUM_ANAGRAMS = 5;
    private static final int DEFAULT_WORD_LENGTH = 3;
    private static final int MAX_WORD_LENGTH = 7;
    private int wordLength = DEFAULT_WORD_LENGTH;
    private Random random = new Random();
    private ArrayList<String> wordList = new ArrayList();
    private HashSet<String> wordSet = new HashSet<>();
    private HashMap<String, List<String>> lettersToWord = new HashMap<>();
    private HashMap<Integer, List<String>> sizeToWords = new HashMap<>();

    public AnagramDictionary(Reader reader) throws IOException {
        BufferedReader in = new BufferedReader(reader);
        String line;
        while((line = in.readLine()) != null) {
            String word = line.trim();
            wordList.add(word);
            wordSet.add(word);
            addToAnagrams(word);
            addToSizeMap(word);
        }
    }

    public boolean isGoodWord(String word, String base) {
        return wordSet.contains(word) && !word.contains(base);
    }

    public List<String> getAnagrams(String targetWord) {
        String key = sortLetters(targetWord);
        return lettersToWord.get(key);
    }

    public List<String> getAnagramsWithOneMoreLetter(String word) {
        ArrayList<String> result = new ArrayList<>();
        for (char c = 'a'; c < 'z'; c++) {
            String wordPlus = word + c;
            String key = sortLetters(wordPlus);
            List<String> anagrams = lettersToWord.get(key);
            if (anagrams != null) {
                for (String anagram : anagrams) {
                    result.add(anagram);
                }
            }
        }
        return result;
    }

    public String pickGoodStarterWord() {
        do {
            List<String> sameLengthWords = sizeToWords.get(wordLength);
            int size = sameLengthWords.size();
            int randomIndex = random.nextInt(size);
            for (int i = randomIndex; i < size; i++) {
                String word = sameLengthWords.get(i);
                String key = sortLetters(word);
                List<String> anagrams = lettersToWord.get(key);
                if (anagrams != null && anagrams.size() >= MIN_NUM_ANAGRAMS) {
                    return word;
                }
            }
            wordLength++;
        } while (wordLength < MAX_WORD_LENGTH);
        return "skate";
    }

    private String sortLetters(String word) {
        char[] chars = word.toCharArray();
        Arrays.sort(chars);
        String sorted = String.valueOf(chars);
        return sorted;
    }

    private void addToAnagrams(String word) {
        String key = sortLetters(word);
        List<String> anagrams = lettersToWord.get(key);
        if (anagrams == null) {
            anagrams = new ArrayList<>();
        }
        anagrams.add(word);
        lettersToWord.put(key, anagrams);
    }

    private void addToSizeMap(String word) {
        int length = word.length();
        List<String> strings = sizeToWords.get(length);
        if (strings == null) {
            strings = new ArrayList<>();
        }
        strings.add(word);
        sizeToWords.put(length, strings);
    }
}
